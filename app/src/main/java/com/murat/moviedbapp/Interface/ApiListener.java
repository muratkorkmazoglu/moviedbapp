package com.murat.moviedbapp.Interface;

public interface ApiListener {
    void success(String strApiName, Object response);
    void error(String strApiName, String error);

    void failure(String strApiName, String message);
}